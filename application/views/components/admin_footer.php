<div class="page-footer">
    <div class="page-footer-inner"> 2015 &copy; Metronic by keenthemes.
        <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<script src="<?php echo ASSETS?>global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS?>global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS?>global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS?>global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS?>global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS?>global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS?>global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo ASSETS?>global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS?>global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo ASSETS ?>global/scripts/sweetalert2.min.js"></script><!--jquery js-->
<script src="<?php echo ASSETS?>global/scripts/app.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS?>layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS?>layouts/layout2/scripts/demo.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS?>layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS ?>global/scripts/jquery.validate.js"></script><!--bootstrap validation js-->
<script src="<?php echo ASSETS ?>global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="<?php echo ASSETS?>global/scripts/intlTelInput.min.js"></script><!--bootstrap validation js-->


<?php isset($script) ? $this->load->view($script) : ''?>
<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>